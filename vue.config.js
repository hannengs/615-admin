const path = require('path')

module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: 'True Protection - Admin',
    },
  },
  lintOnSave: false,
  chainWebpack: (config) => {
    config.resolve.alias.set('@', path.resolve(__dirname, './src'))
  },
}
